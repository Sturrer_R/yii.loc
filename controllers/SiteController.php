<?php

namespace app\controllers;

use app\models\TestForm;
use app\models\UserData;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\User;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSignup()
    {
        $form = new TestForm();
        $user = new UserData();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $feedback_generid = Yii::$app->getSecurity()->generateRandomString(50);
            Yii::$app->session->set('feedback_sess',$feedback_generid);
            $user->name = Yii::$app->request->post('data_name');
            $user->surname = Yii::$app->request->post('data_surname');
            $user->phone = Yii::$app->request->post('data_phone');

            $user->feedbackdataid = $feedback_generid;
            $user->save();


        }

       return $this->render('signup', compact('form'));

    }

    public function actionSignup2()
    {
        $form = new TestForm();
        $user = UserData::findOne(['feedbackdataid'=>Yii::$app->session->get('feedback_sess')]);

        if ($form->load(Yii::$app->request->post())) {
            $user->address = Yii::$app->request->post('data_address');
            $user->save();

        }


    }

    public function actionSignup3()
    {
        $form = new TestForm();
        $user = UserData::findOne(['feedbackdataid'=>Yii::$app->session->get('feedback_sess')]);

        if ($form->load(Yii::$app->request->post())) {
            $user->feedback = Yii::$app->request->post('data_feedback');
            $user->save();

        }

        $response = Yii::$app->response;
        if ($response->statusCode == 200){
            $response->content = "feedbackdataid: " . Yii::$app->session->get('feedback_sess');
        } else {
            Yii::debug($response->statusCode);
        }

    }


}
