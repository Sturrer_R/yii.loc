<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;

?>

<?php Modal::begin([
    'size' => 'modal-md',
    'options' => [
        'id' => 'modal',
    ],
    'toggleButton' => [
        'label' => 'Signup',
        'class' => 'btn btn-success',
        'id' => 'reg_btn',
        'style' => 'margin-left: 470px;padding: 11px;
    font-size: 19px;'
    ],

    'header' => '<h3>Signup window</h3><div id="result"></div>',

]) ?>

<?php $f = ActiveForm::begin(['id' => 'register_form']); ?>

    <fieldset>
        <h2>Step 1: Add Account Details</h2>
        <?= $f->field($form, 'name'); ?>
        <?= $f->field($form, 'surname'); ?>
        <?= $f->field($form, 'phone'); ?>
        <?= Html::button('Next', ['name' => 'next1', 'id' => 'next1', 'class' => 'next-form btn btn-info']); ?>
    </fieldset>

    <fieldset>

        <h2> Step 2: Add address</h2>
        <?= $f->field($form, 'address'); ?>
        <?= Html::button('Previous',
            ['name' => 'previous', 'class' => 'previous-form btn btn-default']); ?>
        <?= Html::button('Next', ['name' => 'next2', 'id' => 'next2', 'class' => 'next-form btn btn-info']); ?>


    </fieldset>


    <fieldset>
        <h2>Step 3: Add Feedback</h2>
        <?= $f->field($form, 'feedback')->textarea(['rows' => 3]); ?>
        <?= Html::button('Previous',
            ['name' => 'previous', 'class' => 'previous-form btn btn-default']); ?>
        <?= Html::submitButton('Submit',
            ['name' => 'submit', 'id' => 'submit', 'class' => 'next-form btn btn-success']); ?>

    </fieldset>

    <fieldset>
        <h2>Result page</h2>
        <div id="msgs">

        </div>
        <div id="result_page">

        </div>
    </fieldset>

<?php ActiveForm::end(); ?>

<?php Modal::end() ?>


<?php
$js = <<<JS

$(document).ready(function(){
  
    var  previous_form, next_form;
        $(".next-form").click(function(){
        previous_form = $(this).parent();
        next_form = $(this).parent().next();
        next_form.show();
        previous_form.hide();
        });
        
        $(".previous-form").click(function(){
        previous_form = $(this).parent();
        next_form = $(this).parent().prev();
        next_form.show();
        previous_form.hide();
        });
    });

    $('#next1').on('click',function() {
          
          var res_data = {
               data_name :  $('#name').val(),
               data_surname : $('#surname').val(),
               data_phone : $('#phone').val()
          }

          $.ajax({
          url: 'signup', 
          data:res_data,
          type: 'POST',
          dataType:'json',
         
          })
          
    });
    
     $('#next2').on('click',function() {
         
              $.ajax({
              url: 'signup2',
              data:  { data_address: $('#address').val() },
              type: 'POST',
               
              })
              
        });
        
         $('#submit').on('click',function() {
             
              $.ajax({
              url: 'signup3',
              data: { data_feedback : $('#feedback').val() },
              type: 'POST',
              
              success: function(response) {
                  if (response.type == "error") {
                      $('#msgs').html("<div class='alert alert-danger'>"+response+"</div>");
                    } else {
                         $('#msgs').html("<div class='alert alert-success'>Your data were successfully saved</div>");
                         $("#result_page").html(response)
                    }
              }
               
              })
              
        });
JS;
$this->registerJS($js);
?>