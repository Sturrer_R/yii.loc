<?php


namespace app\models;


use yii\base\Model;

class TestForm extends Model
{
    public $name;
    public $surname;
    public $phone;
    public $address;
    public $feedback;

    public function rules()
    {
        return [
            [['name'], 'string','length' => [3,255]],
            [['surname'], 'string','length' => [3,255]],
            [['phone'],'match','pattern' => '/^\+380\d{3}\d{2}\d{2}\d{2}$/','message' => 'Incorrect phone number, please use format +380'],
            [['surname'], 'string', 'length' => [3,255]],
            [['address'], 'trim'],
            [['feedback'], 'trim'],
        ];
    }

    public function FormName()
    {
        return "";
    }

}